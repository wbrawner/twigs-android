package com.wbrawner.budget.ui.base

import androidx.compose.ui.graphics.Color

val Green300 = Color(0xFF81C784)
val Green500 = Color(0xFF4CAF50)
val Green700 = Color(0xFF388E3C)
val Green900 = Color(0xFF1B5E20)
val Red300 = Color(0xFFE57373)
val Red500 = Color(0xFFF44336)
val Red700 = Color(0xFFD32F2F)
val Red900 = Color(0xFFB71C1C)

